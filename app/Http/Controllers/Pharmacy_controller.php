<?php

namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Http\Requests;

use Validator;

use DB;

use Auth;

class Pharmacy_controller extends Controller    
  {
    
      public function ini_reg() { 

         return view('ini_reg');
       
       }

   // ---- Fin_reg function started here --- 

    public function reg(request $request) {

               $username = $request ->Name;
      
               $mail = $request ->Email ;
                                        
               $password = $request->Pword ;

               $password = $request->CPword ;

              // $phone_number = $request ->phone_number ;

               $drug_license = $request ->Dlnumber ;

               $token = $request ->_token;
      
     
               $validator=Validator::make($request ->all(),
                                
                                  [          "Name" => 'required | max:50',
                	                           
                                             "Email" => 'required  | max: 50 | email  ',
                                             
                                              "Pword" => 'required | max:30  ',
                                             
                                              "CPword" => 'required | max:30 ',
                                             
                                              //"phone_number" => 'required | numeric',
                                             
                                              "Dlnumber" => 'required |max:40 ']
                       
                                 );
             
                

                if($validator->fails()) {

                        return redirect('ini_reg') -> witherrors($validator) -> withinput();
                          
                          }

                  else{

               
               $form_data = [ 'name' => $username,
                        
                             'email' => $mail,

                             'drug_license_number' => $drug_license,
                        
                             'password' =>bcrypt($password),
                             
                             //'phone' => $phone_number,
                             
                             
                              
                             'Remember_token' => $token 
                              ];



               if(DB::table('ph_user')->insert($form_data)) {

                   return view('profile');
                  
                  }

              else
                  {
                      echo "<strong>Whoopsss</strong> Please check the data";
                     }

          
        }   

        return view('profile');

        }               // ------------> End of Fin_Reg function



      public function profile(request $profile) {

              $pname = $profile ->Pharmacy_Name;
              
              $state = $profile ->State;

              $town = $profile ->Town;

              $district = $profile ->district;

              $about = $profile ->About_pharmacy;

              


              $validator = Validator::make($profile ->all(),[
                                                             'Pharmacy_Name' => 'required | max: 50',
                                                             
                                                             'State' => 'required | alpha | max: 40',
                                                             
                                                             'Town' => 'required | alpha | max: 40',
                                                             
                                                             'district' => 'required | alpha |max : 40',
                                                             
                                                             'About_pharmacy' => 'max:200']);
               
              
              if($validator->fails())
                { 
                  return redirect('Profile_errors') -> witherrors($validator) -> withinput();

                  }

              else 
               {
                      
                   $form_data = [ 'pharmacy_name' => $pname,
                        
                                  'state' => $state,
                             
                                  'town' => $town,

                                  'district' => $district,
                             
                                  'about_pharmacy' => $about ];

                    if(DB::table('pp_profile')->insert($form_data))  {

                      return view('login');
                    }

                    else
                    {
                      echo "error";
                    }       




                  }    

               }

    public function Profile_errors() {
       return view('fin_reg');
    }           



    public function login(){

             return view('login');


    }  

    public function dashboard(request $request ) {
          if(Auth::attempt(['mail' => $request ->username ,'password' =>$request ->password]))
          {
            echo " dashboard " ;
          }

          else
          {
            echo " error ";
          }

       }

    public function search(request $request){

          $vhn= $request->vhn;

          $name = $request ->name;

          $date = $request ->date;

          $status = $request ->status;



        // ####  checking which field is filled and retrieving the result as per the respective field ####

          if($vhn){

                $loop = DB::table('pv_user')
                        ->where('pv_user.vhn_no','=',$vhn)
                        ->count();


                $patient = DB::table('pv_user')                   //to retrieve the result of matching vhn from pv_user
                                ->where('pv_user.vhn_no','=',$vhn)
                                ->join('pv_prescription','pv_prescription.patient_id','=','pv_user.id')
                                ->join('dp_user','dp_user.id','=','pv_prescription.created_by')
                                ->select('pv_user.name as pv_name','pv_user.vhn_no','pv_prescription.rx_no','pv_prescription.processed_status','pv_prescription.created_at','dp_user.name as dp_name')
                                ->get();





           }

           elseif($name){

               $loop = DB::table('pv_user')
                        ->where('pv_user.name','=',$name)
                        ->count();

               $patient = DB::table('pv_user')->where('pv_user.name','=',$name)
                               ->join('pv_prescription','pv_prescription.patient_id','=','pv_user.id')
                               ->join('dp_user','dp_user.id','=','pv_prescription.created_by')
                               ->select('pv_user.name as pv_name','pv_user.vhn_no','pv_prescription.rx_no','pv_prescription.processed_status','pv_prescription.created_at','dp_user.name as dp_name')
                               ->get();


           }

           elseif($date){
               $loop = DB::table('pv_prescription')
                       ->where('pv_prescription.created_at','=',$date)
                       ->count();
               $patient = DB::table('pv_prescription')     // to retrive result accordin to the date
                               ->where('pv_prescription.created_at','=',$date)
                               ->join('pv_user','pv_user.id','=','pv_prescription.patient_id')
                               ->join('dp_user','dp_user.id','=','pv_prescription.created_by')
                               ->select('pv_prescription.rx_no','pv_prescription.created_at','pv_prescription.processed_status','pv_user.name as pv_name','pv_user.vhn_no','dp_user.name as dp_name')
                               ->get();




           }

           elseif($status){


                      if($status = 'opened')
                        {
                           $check = 0;
                           }
                      elseif($status = 'partially Processed'){
                          $check = 1;
                      }

                      else $check = 2;

                 $loop = DB::table('pv_prescription')
                         ->where('pv_prescription.processed_status','=',$status)
                         ->count();

                 $patient = DB::table('pv_prescription')     // to retrive result accordin to the date
                           ->where('pv_prescription.processed_status','=',$check)
                           ->join('pv_user','pv_user.id','=','pv_prescription.patient_id')
                           ->join('dp_user','dp_user.id','=','pv_prescription.created_by')
                           ->select('pv_prescription.rx_no','pv_prescription.created_at','pv_prescription.processed_status','pv_user.name as pv_name','pv_user.vhn_no','dp_user.name as dp_name')
                           ->get();




           }

           return view('dashboard',['patient' => $patient, 'loop' => $loop ]);



    }

     public function prescription()
     {
         $id=1;
         
          

         $loop = DB::table('pv_prescription_details')
             ->where('pv_prescription_details.prescription_id', '=', $id)
             ->count();
			 
         
              
             $drug = "dolo";

             $type = DB::table('pv_prescription_details')
                     ->where('pv_prescription_details.prescription_id', '=',1)
                     ->select('type')
                     ->get();
					 
             $strength = DB::table('pv_prescription_details')
                         ->where('pv_prescription_details.prescription_id', '=',1)
			             ->join('strength_master','strength_master.id', '=', 'pv_prescription_details.strength_id')
                         ->select('strength_master.name')
                         ->get();
           
				 
             $check = DB::table('pv_prescription_details')
                      ->where('pv_prescription_details.prescription_id', '=',1)
				      ->select('drug_type')
					  ->get();

             
             $presentation = DB::table('pv_prescription_details')
                             ->where('pv_prescription_details.prescription_id', '=',1)
			                 ->join('drug_type_master','drug_type_master.id', '=' , 'pv_prescription_details.drug_type')
                             ->select('drug_type_master.name')
                             ->get();
			 
			 

             $schedule = 'TID'; //left for further process

             $SED_1 = DB::table('pv_prescription_details')
                      ->where('pv_prescription_details.prescription_id', '=',1)
					  ->select('start_date')
					  ->get();
            
			$SED_2 = DB::table('pv_prescription_details')
                     ->where('pv_prescription_details.prescription_id', '=',1)
					 ->select('end_date')
					 ->get();
			         

            $RQuantity = DB::table('pv_prescription_details')
                         ->where('pv_prescription_details.prescription_id', '=',1)
						 ->select('qnty')
						 ->get();

            $issue_quantity = DB::table('pv_prescription_details')
                              ->where('pv_prescription_details.prescription_id', '=',1)
							  ->select('issued_qnty')
							  ->get();

             

             $issued_on = DB::table('pv_prescription_details')
                          ->where('pv_prescription_details.prescription_id', '=',1)
						  ->select('issued_on')
						  ->get();

           
             $issued_by = DB::table('pv_prescription_details')
                          ->where('pv_prescription_details.prescription_id', '=',1)
						  ->select('issued_by')
						  ->get();

            

           return view('prescription',['type' => $type , 'strength' => $strength , 'presentation' => $presentation , 'schedule' => $schedule , 'start_date'=> $SED_1 ,
                   'end_date' => $SED_2 , 'required_quantity' => $RQuantity , 'issued_quantity' => $issue_quantity , 'issued_on' => $issued_on ,
                   'issued_by' => $issued_by , 'drug' => $drug , 'loop' => $loop ]);
         
     }











     }




            
            
               





               
             







  



         	

    

  


