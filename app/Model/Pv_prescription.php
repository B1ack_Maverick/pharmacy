<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pv_prescription extends Model
{
    protected $table = 'pv_prescription';
    public function p_details(){


    	return $this->has_one('Pv_prescription_details');
    }

    public function pv_user(){

        return $this->belongsTo('pv_user');


    }

    public function dp_user(){

        return $this->belongsTo('Dp_user');


    }

    public function ph_user(){

        return $this->belongsTo('App\Model\Ph_user');


    }
}
