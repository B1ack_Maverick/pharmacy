<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Drug_Type_Master extends Model
{
    public function drug_type() {

    	return $this->has_one('Pv_prescription_details');
    }
}
