<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Strength_master extends Model
{
    public function strength() {

    	return $this->has_one('Pv_prescription_details');
    }
}
