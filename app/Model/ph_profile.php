<?php

namespace App\Model;

use Illuminate\Database\Eloquent;

class ph_profile extends Eloquent
{
       public $table = 'ph_profile';

       public function user() {

        return $this->belongsto('Ph_user')


       }

       public function country(){

        return $this->has_one('Country_master');

       }

       public function state() {

       	 return $this->has_one('State_master');
       }

       public function city() {

         return $this->has_one('City_master');

       }
}
