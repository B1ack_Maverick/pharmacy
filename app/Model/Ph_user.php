<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ph_user extends model
{   
   protected $table = 'ph_user';
   
   public function prescription() {

   	 return $this->hasmany('App\Model\Pv_prescription','ordered_to');
   }

   public function prescription_detail() {

   	return $this->hasmany('Pv_prescription_details');
   }

   public function profile(){
  
       return $this->hasone('ph_profile');
   }

   public function subscription() {

     return $this->hasone('ph_subscription');

   }

   public function plan() {

    return $this->hasone('App\Model\Ph_plan_master','last_modified_by');

   } 

   
}
 