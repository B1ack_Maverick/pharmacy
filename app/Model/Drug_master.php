<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Drug_master extends Model
{
    public function drug() {

    	return $this->has_one('Pv_prescription_details');
    }
}
