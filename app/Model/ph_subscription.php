<?php

namespace App\Model;

use Illuminate\Database\Eloquent;

class Ph_subscription extends Eloquent
{
    public function user() {
     
     return $this->belongsto('Ph_user');
    }

    public function plan() {

    	return $this->belongsto('Ph_plan_master');
    }
}
