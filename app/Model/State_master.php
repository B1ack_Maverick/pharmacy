<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class State_master extends Eloquent
{  
	public function country(){
   
    return $this->belongsto('Country_master');

    }

    public function city_list(){

    	return $this->has_one('City_master');
    }
}
