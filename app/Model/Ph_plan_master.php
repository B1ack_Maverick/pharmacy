<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ph_plan_master extends Model
{
    protected $table = 'Ph_plan_master';

    public function user(){

    	return $this->belongsto('Ph_user','last_modified_by');
    }

    public function subscription() {

    	return $this->has_one('ph_subscription');
    }
}
