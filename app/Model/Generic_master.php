<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Generic_master extends Model
{
    public function generic() {

    	return $this->has_one('Pv_prescription_details');
    }
}
