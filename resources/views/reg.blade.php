@extends('layouts.master')

@section('title')
 Registration
@endsection 

@section('header')
  Registration
 @endsection 
@section('content')
  {!!Form::Open(array('url' => 'profile'))!!}
  
  <label>Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
  {!!Form::text('Name' , '', array('class' => 'form_input' , 'id' => 'name' , 'placeholder' => 'Name of the Pharmacy'))!!}<br/>
 
  <label>Email:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
  {!!Form::text('Email', '' , array('class' => 'form_input' , 'id' => 'email' , 'placeholder' => 'Email') )!!}<br/>
 
  <label>Password:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
  {!!Form::text('Pword', '' , array('class' => 'form_input' , 'id' => 'pword' , 'placeholder' => '*********') )!!}<br/>
 
  <label>Confirm Password:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
  {!!Form::text('CPword','' , array('class' => 'form_input' , 'id' => 'cpassword' , 'placeholder' => '*******' ))!!}<br/>
 
  <label>Drug License Number:&nbsp;</label>
  {!!Form::text('Dlnumber','' , array('class' => 'form_input' , 'id' => 'drug' , 'placeholder' => 'Drug License Number' ))!!}<br/>
  
  {!!Form::Submit('Register',array('class' =>'button'))!!}

  {!!Form::close()!!}

  @endsection() 