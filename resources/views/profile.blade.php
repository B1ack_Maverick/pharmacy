@extends('layouts.master')

@section('title')
 Profile
@endsection 

@section('header')
  Profile
 @endsection 
@section('content')
  {!!Form::Open(array('url' => 'subscription'))!!}
  
  <label>Profile Logo:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
  {!!Form::file('image' , '', array('class' => 'form_input' , 'id' => 'image' , 'placeholder' => ''))!!}<br/>
 
  <label>Country:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
  {!!Form::text('country', '' , array('class' => 'form_input' , 'id' => 'country' , 'placeholder' => 'Country') )!!}<br/>
 
  <label>State:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
  {!!Form::text('state', '' , array('class' => 'form_input' , 'id' => 'state' , 'placeholder' => 'State') )!!}<br/>
 
  <label>District:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
  {!!Form::text('district','' , array('class' => 'form_input' , 'id' => 'district' , 'placeholder' => 'District' ))!!}<br/>
 
  <label>City:&nbsp;</label>
  {!!Form::text('city','' , array('class' => 'form_input' , 'id' => 'city' , 'placeholder' => 'City' ))!!}<br/>
  
  {!!Form::Submit('Register',array('class' =>'button'))!!}

  {!!Form::close()!!}

  @endsection()