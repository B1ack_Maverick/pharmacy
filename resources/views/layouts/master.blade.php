<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>@yield('Title')</title>
<link rel='stylesheet' type="text/css" href="{{URL::to('\style.css')}}"  >
   

</head>
<body>
    
    <div class="container">
        
       
          <div class="form">
              <h1 class="header">@yield('header')</h1>
              <div class="line"></div>
               
                @yield('content')
           </div>
    </div>
    
    
    
    
    
    
    </body>    
    
    
</html>    
