<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>@yield('Title')</title>
<link rel='stylesheet' type="text/css" href="{{URL::to('\style.css')}}"  >
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
   

</head>
<body>
	

	<header>@yield('heading')</header>
    
    <div class="container">
       
       <div class = "search_box">
          @yield('searchbox');
          </div>

       <div class ="content">
         @yield('content');
         </div>   
       

        
          


       
          
    </div>
    
    
    
    
    
    
    </body>    
    
    
</html>    