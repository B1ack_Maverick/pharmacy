<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Details</title>
<link rel='stylesheet' type="text/css" href="{{URL::to('\style.css')}}"  >
   

</head>
<body>
    
    <div class="container">
        
        <div class="dash_cont">
           
           <table>
           	<tr> <th></th>
           	 
           	 <th>Drug</th>
           	 <th>Type</th>
           	 <th>Strength</th>
           	 <th>Presentation</th>
           	 <th>Schedule</th>
           	 <th>Start End Date</th>
           	 <th>Required Quantity</th>
           	 <th>Issue Status</th>
           	 <th>Issued Quantity</th>
           	 <th>Remaining</th>
           	 <th>Issued On</th>
           	 <th>Issued By</th>

           	</tr>
           	@for($i=0 ; $i<$val ; $i++ )
           	 <tr>
               <td>$drug[$i]</td>
               <td>$type[$i]</td>
               <td>$strength[$i]</td>
               <td>$presentation[$i]</td>
               <td>$schedule[$i]</td>
               <td>$sed[$i]</td>
               <td>$rqr_qty[$i]</td>
               <td>$issue_status[$i]</td>
               <td>$issued_qnty[$i]</td>
               <td>$remaining[$i]</td>
               <td>$Issued_on[$i]</td>
               <td>$issued_by[$i]</td>
           	 </tr>
            @endfor




        </div>

       
          
    </div>
    
    
    
    
    
    
    </body>    
    
    
</html>    