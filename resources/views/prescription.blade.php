@extends('layouts.master')

@section('title')
    Details
@endsection

@section('content')
   <table class="prescription_table">

        <tr>
             <th>Drug</th>
             <th>Type</th>
             <th>Strength</th>
             <th>Presentation</th>
             <th>Schedule</th>
             <th>Start - End - Date</th>
             <th>Required Quantity</th>
             <th>Issue Status</th>
             <th>Issued Quantity</th>
             <th>Remaining</th>
             <th>Issued On</th>
             <th>Issued By</th>
        </tr>
    @for($i=0;$i<$loop;$i++)
       <tr>
             <td><?php echo $drug?></td>

             <td><?php echo "general" ?></td>

             <td><?php echo  $strength[0]?></td>

             <td><?php echo  $presentation[0]?></td>

             <td><?php echo  $schedule?></td>

             <td><?php echo  $start_date[0]. 'to'. $end_date[0]?></td>

             <td><?php echo  $required_quantity[0]?></td>

             <td><?php if($required_quantity[0] == $issued_quantity[0])'Issued'?></td>

             <td><?php echo  $issued_quantity[0]?></td>

             <td><?php echo  $required_quantity[0] - $issued_quantity[0]?> </td>

             <td><?php  echo $issued_on[0]?></td>

             <td><?php echo  $issued_by[0]?></td>

        </tr>

    @endfor
   </table>

@endsection()