<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PhProfile extends Migration
{
    
    public function up()
    {
        Schema::create('ph_profile',function(Blueprint $table)
        {

          $table ->increments('id');

          $table ->integer('user_id') ->unsigned();

          $table ->foreign('user_id')->references('id')->on('ph_user');

          $table ->string('logo_path');

          $table ->integer('country_id') ->unsigned();

          $table ->foreign('country_id')->references('id')->on('country_master');
          
          $table ->integer('state_id') ->unsigned();
          
          $table ->foreign('state_id')->references('id')->on('state_master');

          $table ->integer('city_id') ->unsigned();

          $table ->foreign('city_id')->references('id')->on('city_master');
          
          $table ->text('about');

          $table ->integer('active_yesno');

          $table ->integer('last_modified_by');

          $table ->rememberToken();
               
               $table ->timestamps();







        });
        
    }

    
    public function down()
    {
        Schema::drop('ph_profile');
    }
}
