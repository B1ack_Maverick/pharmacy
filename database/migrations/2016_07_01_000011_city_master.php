<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CityMaster extends Migration
{
    
    public function up()
    {
        Schema::create('city_master',function(Blueprint $table){
         
         $table->increments('id') ->unique();

         $table ->string('name');

         $table ->integer('state_id')->unsigned();

         $table->foreign('state_id')->references('id')->on('state_master');

         $table ->integer('active_yesno');

         $table ->timestamps();



        });
    }

    
    public function down()
    {
        Schema::drop('city_master');

    }
}
