<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PvPrescriptionDetails extends Migration
{
   
    public function up()
    {
        Schema::create('pv_prescription_details',function(Blueprint $table) {
         
         $table ->increments('id');

         $table ->integer('prescription_id')->unsigned();

         $table->foreign('prescription_id')->references('id')->on('pv_prescription');

         $table ->integer('type');

         $table ->integer('drug_id')->unsigned();

         $table->foreign('drug_id')->references('id')->on('drug_master');

         $table ->integer('generic_id')->unsigned();

         $table->foreign('generic_id')->references('id')->on('generic_master');

         $table ->text('free_text');

         $table ->string('dose'); 

         $table ->integer('strength_id')->unsigned();

         $table->foreign('strength_id')->references('id')->on('strength_master');

         $table ->integer('drug_type')->unsigned();

         $table->foreign('drug_type')->references('id')->on('drug_type_master');

         $table ->integer('qnty');

         $table ->string('schedule');

         $table ->date('start_date');
 
         $table ->date('end_date');

         $table ->text('instructions');

         $table ->integer('processed_status');

         $table ->integer('processed_by');

         $table ->integer('issued_qnty');

         $table ->date('issued_on');

         $table ->integer('issued_by')->unsigned();

         $table->foreign('issued_by')->references('id')->on('ph_user');

         $table ->timestamps();
        });
    }

    
    public function down()
    {
        Schema::drop('pv_prescription_details');
    }
}
