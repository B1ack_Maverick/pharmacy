<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StrengthMaster extends Migration
{
    
    public function up()
    {
        Schema::create('strength_master',function(Blueprint $table){
         

        $table ->increments('id') ->unique();

        $table ->string('name');

        $table ->integer('active_yesno');

        $table ->integer('last_modified_by');

        $table ->integer('last_modified_date_time');



        });
    }

    
    public function down()
    {
        Schema::drop('strength_master');
    }
}
