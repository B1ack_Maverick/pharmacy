<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StateMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('state_master',function(Blueprint $table){
         
         $table->increments('id') ->unique();

         $table ->integer('country_id') ->unsigned();

         $table->foreign('country_id')->references('id')->on('country_master');

         $table ->integer('active_yesno');

         $table ->timestamps();



        });
    }

    public function down()
    {
        Schema::drop('state_master');
    }
}

