<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/*  
           When Dp_visit table is available add it in the class

             $table ->integer('visit_id')->unsigned();

             $table->foreign('visit_id')->references('id')->on('dp_visit');


*/            

class PvPrescription extends Migration
{
    
    public function up()
    {
        Schema::create('pv_prescription',function(Blueprint $table)
        {
            $table ->increments('id');

            $table ->string('rx_no');

            $table ->integer('patient_id')->unsigned();

            $table->foreign('patient_id')->references('id')->on('pv_user');

           

            $table ->integer('created_by')->unsigned();

            $table->foreign('created_by')->references('id')->on('dp_user');

            $table ->integer('processed_status');

            $table ->integer('shared_yesno');
            
            $table->timestamps();

            $table ->integer('ordered_to')->unsigned();

            $table->foreign('ordered_to')->references('id')->on('ph_user');

            
       });
    }

    
    public function down()
    {
        Schema::drop('pv_prescription');
    }
}
