<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DpUser extends Migration
{
    
    public function up()
    {
       Schema::create('dp_user',function(Blueprint $table){
        
        $table ->increments('id');

        $table ->string('name');

        $table ->integer('active_yesno');

        $table ->integer('last_modified_by');

        $table ->timestamps();

       });
    }

    
    public function down()
    {
        Schema::drop('dp_user');
    }
}
