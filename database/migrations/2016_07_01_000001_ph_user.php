<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PhUser extends Migration
{
    
    public function up()
    {
        Schema::create('ph_user',function(Blueprint $table){
               
               $table->increments('id');

               $table ->string('name');
               
               $table ->string('email') ->unique();
               
               $table ->string('drug_license_number') ->unique();
               
               $table ->string('password');

               $table ->integer('active_yesno');

               $table ->integer('last_modified_by');

               $table->rememberToken();
               
               $table->timestamps();

});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ph_user');
    }
}
