<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PhSubscription extends Migration
{
    
    public function up()
    {
       Schema::create('ph_subscription',function(Blueprint $table){

          $table ->increments('id');

          $table ->integer('user_id')->unsigned();

          $table ->foreign('user_id')->references('id')->on('ph_user');
          
          $table ->integer('plan_id')->unsigned();

          $table ->foreign('plan_id')->references('id')->on('ph_plan_master');

          $table ->date('start_date');

          $table ->date('expiry_date');

          $table ->integer('active_yesno');

          $table ->integer('last_modified_by')->unsigned();

          $table ->foreign('last_modified_by') ->references('id') ->on('ph_user');

          $table->rememberToken();
               
          $table->timestamps();



       });
    }

   
    public function down()
    {
        Schema::drop('ph_subscription');
    }
}
