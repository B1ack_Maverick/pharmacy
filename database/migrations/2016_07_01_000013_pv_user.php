<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PvUser extends Migration
{

    public function up()
    {
        Schema::create('pv_user',function(Blueprint $table){
        
        $table ->increments('id');

        $table ->string('name');

        $table ->string('email');

        $table ->integer('phone_number')->unique();

        $table ->string('vhn_no')->unique();

        $table ->string('password');

        $table ->string('activation_token');

        $table ->integer('activation_attempts');

        $table ->integer('active_yesno');

        $table ->dateTime('last_modified_date_time');

       });
    }

    
    public function down()
    {
        Schema::drop('pv_user');
    }
}
