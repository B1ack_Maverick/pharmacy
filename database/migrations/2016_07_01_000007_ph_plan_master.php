<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PhPlanMaster extends Migration
{
    
    public function up()
    {
        Schema::create('ph_plan_master',function(Blueprint $table){
         
          $table->increments('id');

          $table ->string('plan_name') ->unique();
               
          $table ->string('validity');

          $table ->integer('active_yesno');

          $table ->integer('last_modified_by') ->unsigned();

          $table ->foreign('last_modified_by')->references('id') ->on('ph_user');

          $table ->timestamps();







        });
    }

   
    public function down()
    {
        Schema::drop('ph_plan_master');
    }
}
