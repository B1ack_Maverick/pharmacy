<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CountryMaster extends Migration
{
    
    public function up()
    {
        Schema::create('country_master',function(Blueprint $table){
         
         $table->increments('id');

         $table ->string('name');

         $table ->integer('active_yesno');

         $table ->timestamps();



        });
    }

    
    public function down()
    {
        Schema::drop('country_master');
    }
}
